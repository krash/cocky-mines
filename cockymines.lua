function widget:GetInfo()
	return {
		name = "Cocky Mines v1.0",
		desc = "Installs the group of mines nearby with 1 mouse click",
		author = "Leonid Krashenko <leonid.krashenko@yandex.ru>",
		date = "Mar 08, 2018",
		license = "GNU GPL, v2",
		layer = 0,
		enabled = true --  loaded by default?
	}
end

local BUILD_RADIUS = 150
local AMOUNT = 15

local spGetMyPlayerID = Spring.GetMyPlayerID
local spGetPlayerInfo = Spring.GetPlayerInfo
local spGetMyTeamID = Spring.GetMyTeamID
local spGetTeamUnits = Spring.GetTeamUnits
local spGetUnitDefID = Spring.GetUnitDefID
local echo = Spring.Echo

local mousePressed = 0 -- mouse press flag
local mlvs = {} -- player's mlvs
local armMLV = UnitDefNames["armmlv"].id
local coreMLV = UnitDefNames["cormlv"].id
local armEyes = UnitDefNames["armeyes"].id  -- ARM camera
local coreEyes = UnitDefNames["coreyes"].id  -- CORE camera

math.randomseed(os.time())


local function dispatchUnit(unitID, unitDefID)
	if unitDefID == armMLV or unitDefID == coreMLV then
		local udef = UnitDefs[unitDefID]
		mlvs[unitID] = unitID
	end
end


function widget:UnitCommand(unitID, unitDefID, teamID, cmdID, cmdParams, cmdOptions)
	-- if unit is not mine layer then return
	if mlvs[unitID] ~= unitID then
		return
	end

	-- if mine layer's command is to build camera then return
	-- (we don't want to build a field of cameras)
	if cmdID == -armEyes or cmdID == -coreEyes then
		return
	end
	
	-- mousePressed is a "mouse release" flag to prevent recursive command execution
	-- (we are giving orders from the UnitCommand callin - they shall produce
	-- new UnitCommand callins and so on... so we are only interested in
	-- commands given with mouse click, not programmatically)
	local pos = cmdParams
	if cmdID < 0 and mousePressed == 1 then
		mousePressed = 0
		local cx = pos[1]
		local cy = pos[3]
		local r = BUILD_RADIUS
		
		for i=1, (AMOUNT-1) do
			local rx = (math.random(0, 20) - 10)/10;
			local ry = (math.random(0, 20) - 10)/10;

			local tx = cx + rx*r;
			local ty = cy + ry*r;

			Spring.GiveOrderToUnit(unitID, cmdID, { tx, 0, ty }, {"shift"})
		end

	end
end


function widget:MousePress(mx, my, button)
	mousePressed = 1
end


function widget:UnitCreated(unitID, unitDefID, unitTeam, builderID)
	if (unitTeam ~= spGetMyTeamID()) then
		return
	end
	dispatchUnit(unitID, unitDefID)
end


function widget:UnitDestroyed(unitID, unitDefID, unitTeam)
	mlvs[unitID] = nil
end


function widget:UnitTaken(unitID, unitDefID, unitTeam, newTeam)
	widget:UnitDestroyed(unitID, unitDefID)
end


function widget:UnitGiven(unitID, unitDefID, unitTeam, oldTeam)
	widget:UnitCreated(unitID, unitDefID, unitTeam, 0)
end


function widget:Initialize()
																																						
	local playerID = spGetMyPlayerID()
	local _, _, spec, _, _, _, _, _ = spGetPlayerInfo(playerID)
	if spec == true then
		Spring.Echo("<Cocky Mines> Spectator mode. Widget removed")
		widgetHandler:RemoveWidget(self)
	end

	local allunits = spGetTeamUnits(spGetMyTeamID())
	for _, uid in ipairs(allunits) do
		dispatchUnit(uid, spGetUnitDefID(uid))
	end
end
